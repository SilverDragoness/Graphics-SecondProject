﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberOfPlayers : MonoBehaviour 
{
	public Slider playerSlider;

	float numberOfPlayes;

	void Start()
	{
		numberOfPlayes = 1f;
		playerSlider.onValueChanged.AddListener (delegate
		{
			SetNumberOfPlayers();
		});

	}
		
	public void SetNumberOfPlayers()
	{
		numberOfPlayes = playerSlider.value;//sliderValue;
	}


	// This function is called in order to get the number of players.

	public float GetNumberOfPlayers()
	{
		return numberOfPlayes;
	}
}
