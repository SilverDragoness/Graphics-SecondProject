﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallCameraController : MonoBehaviour 
{
	public GameObject ball;
	Vector3 offset;
	public bool follow;

	void Start () 
	{
		// Set the starting offset for the camera.

		offset = transform.position - ball.transform.position;
	}
	
	// Update is called once per frame
	// Runs after all items is being processed.	
	void LateUpdate () 
	{
		if (follow)
		{
			// Make sure that the camera follows the ball with the current offset.

			transform.position = ball.transform.position + offset;

		}
	}
}
