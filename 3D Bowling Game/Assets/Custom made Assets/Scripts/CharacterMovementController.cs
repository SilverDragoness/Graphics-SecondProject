﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovementController : MonoBehaviour
{
	float movingSpeed = 1.5f; 
	Vector3 moveDirection;// = new Vector3(9.0f, 1.0f, 0.0f);
	public GameObject guiManager;
	UIManager uiManager;

	// In order to unstuck if something happened.

	Transform resetTransform;

	void Start()
	{
		//GetComponent<Rigidbody> ().isKinematic = true;
		uiManager = guiManager.GetComponent<UIManager> ();
		//transform.position = new Vector3(13.0f, 1.0f, 0.0f);
	}

	void Update()
	{
		
	}

	// Use this for initialization.
	// We want to check and apply each input.

	void FixedUpdate () 
	{
		if (!uiManager.GetAimUIStatus())
		{
			CheckForCharacterMovementInput ();
		}

	}

	void CheckForCharacterMovementInput()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal"); // -1 0 1
		float moveVertical = Input.GetAxis ("Vertical"); // -1 0 1

		moveDirection = new Vector3 (moveVertical * -1, 0.0f, moveHorizontal);
		moveDirection = transform.TransformDirection (moveDirection);
		moveDirection *= movingSpeed;
		transform.position += moveDirection * Time.deltaTime;

		//character.moveDirection = moveDirection * Time.deltaTime;
	}
		
	void OnCollisionEnter(Collision withObject)
	{
		if (withObject.gameObject.name == "Alley")
		{
			//Debug.Log ("OUps");
		}
	}

	void OnColiisionStay(Collision withObject)
	{
		if (withObject.gameObject.name == "Alley")
		{
			//Debug.Log ("OUps");

			//Rigidbody rb = GetComponent<Rigidbody> ();

			//rb.constraints = RigidbodyConstraints.None;
		}
	}
}
