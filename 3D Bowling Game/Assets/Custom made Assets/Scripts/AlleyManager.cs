﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlleyManager : MonoBehaviour 
{
	// This class is aware of the progress of the game.
	// This class is also responsible for updating the screens.
	public Rigidbody[] kegelGroup;
	public Rigidbody[] ballsGroup;
	GameObject thrownBall;
	Vector3[] kegelPossitions;
	int numberOfRounds; // Max number of rounds is 10.
	int ballOrder;
	int score;

	// Use this for initialization

	void Start () 
	{
		numberOfRounds = 0;
		ballOrder = 1;
		score = 0;
		kegelPossitions = new Vector3[10];
		thrownBall = null;

		for (int i = 0; i < 10; i++) 
		{
			kegelPossitions [i] = new Vector3(kegelGroup [i].transform.localPosition.x, kegelGroup [i].transform.localPosition.y, kegelGroup [i].transform.localPosition.z);
		}
	}

	// Update is called once per frame
	void Update () 
	{
		
	}

	// This function is called when an object has entered the alleys trigger collider.

	void OnTriggerEnter(Collider objectCollider)
	{
		if (objectCollider.gameObject.name == "Ball")//isBeingHit == false)
		{
			//thrownBall = objectCollider.gameObject;
			
			// The game has started.

			numberOfRounds += 1;

			//Debug.Log ("The frame has started. Ball is thrown and entered the alley.");

			GetComponent<AudioSource> ().PlayDelayed (0.6f);
		}

		if (objectCollider.gameObject.name == "Character")//isBeingHit == false)
		{
			// This is a foul. The alley resets and the score counts 0.

			// Inform the GameManager.

			GetComponent<GameManager> ().UpdateScore (0);

			AlleyReset ();
		}
	}
		
	// This function is called when an object has exited the alleys trigger collider.

	void OnTriggerExit(Collider objectCollider)
	{
		// Its time to calculate the score.

		if (objectCollider.gameObject.name == "Ball")
		{
			// We must also wait some seconds in order to be more precise.

			//Debug.Log (" Ball is thrown and exited the alley.");

		}
	}

	void AlleyReset()
	{
		for (int i = 0; i < 10; i++) 
		{
			KegelManager  kegelManager = kegelGroup[i].gameObject.GetComponent<KegelManager>();

			kegelManager.KegelReset();
		}
	}

	void AlleyCleanUp()
	{
		for (int i = 0; i < 10; i++) 
		{
	
			KegelManager  kegelManager = kegelGroup[i].gameObject.GetComponent<KegelManager>();

			kegelManager.KegelClean();

		}

		// Tell also the ball to reset.

		ballsGroup [0].SendMessage ("BallReset");
	}

	IEnumerator CountKegels()
	{
		//Debug.Log ("Wait a bit");

		yield return new WaitForSeconds(4);

		//Debug.Log ("Count the kegels.");

		// At first we must get the number of kegels that have been nocked over.

		int numberOfFallenKegels = 0;

		// For each kegel in the group.

		for (int i = 0; i < 10; i++) 
		{
			// Access the kegel manager script of that kegel.

			KegelManager kgManager = kegelGroup [i].GetComponent<KegelManager> ();

			// Check if that kegel is down.

			if (kgManager.GetStatus() && kgManager.GetComponent<Rigidbody>().isKinematic == false)
			{
				// Increment the number of downed kegels.

				numberOfFallenKegels += 1;
			}
		}
		// Inform the GameManager.

		GetComponent<GameManager> ().UpdateScore (numberOfFallenKegels);

		AlleyCleanUp ();
	}


	public void ballEnteredNoCameraArea()
	{
		// Start the coroutine in order to count the kegels.

		StartCoroutine (CountKegels());
	}
}
