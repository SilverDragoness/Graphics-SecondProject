﻿//using System; -> UnityEngine
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	// This class is responsible for managing the game variables and the game mobes.

	// The number of players in the game that is about to being it can go from 1 to many. Also in the feature we can add 0 for ai

	public GameObject guiManager;
	public Canvas storingCanvas;
	public int numberOfPlayers;
	public int numberOfBowlingBalls;
	public int numberOfAIPlayers;
	public int gameMode; // Standart , random and hazard.
	public string[] playerNames;
	//Object framePanelPrefab;
	public GameObject framePanelPrefab;
	// An array that stores the number of scoreManagers.

	ScoreManager[] scoreManagerArray;
	//ScorePresenter[] scorePresenterArray;
	GameObject[] framePanelPrefabArray;
	int numberOfTotalPlayers;
	int frame;
	int playerTurn;
	bool restart;
	UIManager uiManager;

	//framePanel = Instantiate (framePanelPrefab, storingCanvas.transform.position, storingCanvas.transform.rotation);
	// At start we will make many objects of scoreManagers and feed them the throws based on the players turn.
	// At the end we will take the final score of each and find the winner.
	// Also this class informs the ui in order to show which player turn is it.

	void Start ()
	{
		// Get the ui manager from the GUIObject

		uiManager = guiManager.GetComponent<UIManager> ();

		GameObject objectFromPreviousScene = GameObject.Find ("ValueStore");
		NumberOfPlayers numberOfPlayersScript = objectFromPreviousScene.GetComponent<NumberOfPlayers> ();
	
		numberOfPlayers = Mathf.FloorToInt (numberOfPlayersScript.GetNumberOfPlayers ());

		// Destroy the object from the previous scene.

		Destroy (objectFromPreviousScene);

		// Also we need to instansiate a number of frame prefabs that have on their own score Presenter based on the players

		InitializeFramePanelPrefabArray ();

		// Call the function that handles the start and restarts.

		Restart ();
	}

	// This function is called in order to restart the game with exactly the same parameters(Players AI balls etc.)

	void Restart()
	{
		restart = false;
		frame = 0;
		scoreManagerArray = new ScoreManager[numberOfPlayers];

		for(int i = 0; i < numberOfPlayers; i++)//each (int player in numberOfPlayers)
		{
			scoreManagerArray [i] = new ScoreManager ();
		}
			
		for(int i = 0; i < numberOfPlayers; i++)//each (int player in numberOfPlayers)
		{
			framePanelPrefabArray [i].GetComponent<ScorePresenter>().Restart ();
			framePanelPrefabArray [i].GetComponent<ScorePresenter>().SetupPlayerName ("Player"+(i+1));
		}
	}

	void InitializeFramePanelPrefabArray()
	{
		framePanelPrefabArray = new GameObject[numberOfPlayers];

		float distance = -80.0f;

		for(int i = 0; i < numberOfPlayers; i++)//each (int player in numberOfPlayers)
		{
			GameObject scoreGUIScrollViewViewportContent = GameObject.FindGameObjectWithTag ("ScoreGUIScrollViewViewportContent");

			Vector3 offset = new Vector3 (0.0f, distance, 0.0f);

			//Debug.Log (scoreGUIScrollViewViewport.GetComponent<RectTransform> ());

			GameObject newPrefab = Instantiate (framePanelPrefab, scoreGUIScrollViewViewportContent.transform.position + offset, scoreGUIScrollViewViewportContent.transform.rotation);

			//Debug.Log (scoreGUIScrollViewViewport.transform.position);
			distance -= 100.0f;

			newPrefab.transform.SetParent (scoreGUIScrollViewViewportContent.transform);

			framePanelPrefabArray [i] = newPrefab;
		}

		// Since its the restart or start set active the first player.

		framePanelPrefabArray [0].GetComponent<ScorePresenter>().ActivateFrame ();
	}

	// In the update method the game manager check for safe guards.

	void Update () 
	{ 
		if (restart)
		{
			if (Input.GetKey (KeyCode.R))
			{
				Debug.Log ("Restart!");

				// Show restart text.

				// We need to inform the alley manager to reset the alley since that players turn has finished.

				GetComponent<AlleyManager> ().SendMessage ("AlleyCleanUp");

				GetComponent<AlleyManager> ().SendMessage ("AlleyReset");

				Restart (); // Or wait for a key press and restart?

				// Inform the uiManager.

				uiManager.SetGameOver (false);
			}
				
		}

		// Check if the user has pressed esc in order to exit to the main menu.

		if (Input.GetKey (KeyCode.Escape))
		{
			// Set the cursor to visible.

			Cursor.visible = true;

			// And load menu scene.

			SceneManager.LoadScene (0);// ,LoadSceneMode.Additive);
		}
	}

	// This function is called when the game manager need to infrom one of the score managers that there was a new throw.

	public void UpdateScore(int numberOfKegelsKnockedDown)
	{
		// Inform the appropriate score manager.

		scoreManagerArray[playerTurn].EvaluateNewThrow(numberOfKegelsKnockedDown);//("EvaluateNewThrow", d);

		// Inform the appropriate score presenter.

		framePanelPrefabArray [playerTurn].GetComponent<ScorePresenter>().UpdateThrowValue (numberOfKegelsKnockedDown);

		// Check if the alley needs reset. Most of the times unless that manager is on the last frame.
		// Every time the alley needs return we need to change the player turn.

		print((scoreManagerArray[playerTurn].GetReset()));

		if (scoreManagerArray[playerTurn].GetReset() == true)
		{
			// We need to inform the alley manager to reset the alley since that players turn has finished.

			GetComponent<AlleyManager> ().SendMessage ("AlleyReset");

			// Check the players turn. If its not the last player.

			if (playerTurn < (numberOfPlayers + numberOfAIPlayers - 1))
			{
				// Since the player has finished his turn his score needs to be updated as well.

				framePanelPrefabArray [playerTurn].GetComponent<ScorePresenter>().UpdateFrameScore (scoreManagerArray[playerTurn].GetScoreArray());

				// Deactivate the previous player's frame.

				framePanelPrefabArray [playerTurn].GetComponent<ScorePresenter>().DeactivateFrame ();
				
				// Change the player turn.

				playerTurn += 1;

				// Activate the new player's frame.

				framePanelPrefabArray [playerTurn].GetComponent<ScorePresenter>().ActivateFrame ();

			}
			else //if (playerTurn == (numberOfPlayers + numberOfAIPlayers - 1)) // If its the last player.
			{
				// Since the player has finished his turn his score needs to be updated as well.

				framePanelPrefabArray [playerTurn].GetComponent<ScorePresenter>().UpdateFrameScore (scoreManagerArray[playerTurn].GetScoreArray());

				// Deactivate the previous player's frame.

				framePanelPrefabArray [playerTurn].GetComponent<ScorePresenter>().DeactivateFrame ();

				// Reset the player turn. 

				playerTurn = 0;

				// Check if its the last frame.

				// Activate the new player's frame.

				framePanelPrefabArray [playerTurn].GetComponent<ScorePresenter>().ActivateFrame ();

				// If not.

				if (frame < 9)
				{
					// Increment the frame.

					frame += 1;
				}
				else
				{
					// Check if for that player is a game over. Meaning that he/ she has thrown tha last ball.

					Debug.Log (scoreManagerArray [playerTurn].GetGameOver ());

					if (scoreManagerArray [playerTurn].GetGameOver ())
					{

						framePanelPrefabArray [playerTurn].GetComponent<ScorePresenter>().UpdateFinalFrame (scoreManagerArray[playerTurn].GetScore());

						// Call the function that check for the winner.

						CheckForWinner ();

						// Inform the uiManager.

						uiManager.SetGameOver (true);

						restart = true;

					}
					else
					{
						// The game isnt over yet. More player can play so keep the turn rolling.

						playerTurn = 0;
					}
				}
			}
			//PrintProgress ();
		}
	}

	// This is a debugg function that prints the progress.

	void PrintProgress()
	{
		Debug.Log ("Number of players: " + numberOfPlayers);
		Debug.Log ("Number of bowlingBalls: " + numberOfBowlingBalls);
		Debug.Log ("Number of AI players: " + numberOfAIPlayers);
		Debug.Log ("Game mode: " + gameMode);
		Debug.Log ("PLayer Names: " + playerNames);
		Debug.Log ("Bowlling frame: " + frame);
		Debug.Log ("Player turn: " + playerTurn + " Named: - ");
	}

	void CheckForWinner()
	{
		// Initialize the max score. // Score - player id

		int[] scoreArray = new int[scoreManagerArray.GetLength (0)];
		int[] idArray = new int[scoreManagerArray.GetLength (0)];
		int maxScore = -1;
		int playerId = -1;
		int secondMaxScore = -1;
		bool isTie = false;

		//Array.sort
		// For each score manager check the score and find the max.

		for (int i = 0; i < scoreManagerArray.GetLength (0); i++)
		{
			scoreArray [i] = scoreManagerArray [i].GetScore ();
			idArray [i] = i;	

		}

		// Sort the array.

		System.Array.Sort (scoreArray, idArray);

		// Reverse the two arrays since they sort doesnt do exactly what we want.

		System.Array.Reverse(scoreArray);
		System.Array.Reverse(idArray);

		// Check if there are more than one player.

		if (numberOfPlayers + numberOfAIPlayers > 1)
		{
			if (scoreArray [0] == scoreArray [1])
			{
				uiManager.SetupWinnerText ("There is a tie!");
			}
			else
			{
				uiManager.SetupWinnerText ("The winner is: Player" + (idArray [0]+1) + "with score: " + scoreArray [0]);
			}
		}
		else
		{
			uiManager.SetupWinnerText ("Game Over Player" + (idArray [0]+1) + ". Your score is: " + scoreArray [0]);
		}

	}


	// This method is called in order to update the UI elements. (Screens and tabScore)

	void UpdateUIElements()
	{
		
	}
		

}
