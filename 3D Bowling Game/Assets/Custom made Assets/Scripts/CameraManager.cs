﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour // Man?
{
	// The cameras that the camera man is handling based on the key the player is pressing 1 - 4

	public Camera characterCamera;
	public Camera bowlingBallCamera;
	public Camera alleyCamera;
	public Camera angledAlleyCamera;
	public GameObject ball;

	BallManager ballManager;

	// The public function in order to change the cameras based on the user's key presses.

	void Start()
	{
		ballManager = ball.GetComponent<BallManager> ();
	}

	// Update is called once per frame

	void Update () 
	{
		CheckCameraInputs ();
	}

	// This function checks the user input in switch the cameras.

	void CheckCameraInputs()
	{
		// Check if the user pressed the button in order to throw the ball. 

		if (Input.GetButtonDown ("Camera1")) // Fire1 stands for left shift.
		{
			// Change the camera to character.

			ShowCharacterCamera ();
		}
		else if (Input.GetButtonDown ("Camera2")) //&& ballManager.GetThrownStatus()) // Fire1 stands for left shift.
		{
			// Change the camera to ball.

			ShowBowlingBallCamera ();
		}
		else if (Input.GetButtonDown ("Camera3")) // Fire1 stands for left shift.
		{
			// Change the camera to top camera.

			ShowAlleyTopCamera ();
		}
		else if (Input.GetButtonDown ("Camera4")) // Fire1 stands for left shift.
		{
			// Change the camera to alley camera.

			ShowAngledAlleyCamera ();
		}
	}


	// This function enables the character's camera.

	public void ShowCharacterCamera()
	{
		characterCamera.enabled = true;
		bowlingBallCamera.enabled  = false;
		alleyCamera.enabled  = false;
		angledAlleyCamera.enabled  = false;
	}

	// This function enables the ball's camera.

	public void ShowBowlingBallCamera()
	{
		characterCamera.enabled  = false;
		bowlingBallCamera.enabled  = true;
		alleyCamera.enabled  = false;
		angledAlleyCamera.enabled  = false;
	}

	// This function enables the alley's top camera.

	public void ShowAlleyTopCamera()
	{
		characterCamera.enabled  = false;
		bowlingBallCamera.enabled  = false;
		alleyCamera.enabled  = true;
		angledAlleyCamera.enabled  = false;
	}

	// This function enables the alley's angled camera.

	public void ShowAngledAlleyCamera()
	{
		characterCamera.enabled  = false;
		bowlingBallCamera.enabled  = false;
		alleyCamera.enabled  = false;
		angledAlleyCamera.enabled  = true;
	}
}
