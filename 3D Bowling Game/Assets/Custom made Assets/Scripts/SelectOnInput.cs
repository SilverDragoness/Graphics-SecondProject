﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectOnInput : MonoBehaviour 
{
	public EventSystem eventSystem;
	public GameObject selectedObject;

	// Update is called once per frame

	private bool buttonSelected;

	void Update () 
	{
		// Check if the user tries to navigate via keyboard in order to select for him the first button/slider.

		if (Input.GetAxisRaw ("Vertical")!=0 && buttonSelected == false)
		{
			eventSystem.SetSelectedGameObject (selectedObject);
			buttonSelected = true;
		}
	}

	// On the object disable make sure to reset the bool so if/when the user comes back there wont be any selected object by default.

	void OnDisable()
	{
		buttonSelected = false;
	}
}
