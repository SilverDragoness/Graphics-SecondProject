﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallThrowingManager : MonoBehaviour 
{
	// Declare the public variables

	public GameObject cameraMan;
	public GameObject guiManager;
	//public GameObject ballLauncher;
	public Rigidbody ball; // This could be changed based onthe ball the player pick up.
	public Camera ballCamera; //= GetComponentInChildren<Camera> ();//GetComponent<Camera>();
	public Slider forceSlider;
	public Slider leftSpinSlider;
	public Slider rightSpinSlider;

	float throwPower; //= 100f;// 10-130
	//float oldPower = 18f;
	//public GUITexture crosshair;

	// There should be different aproaches for example if the ball is picked up or not etc.
	UIManager uiManager;
	// Use this for initialization.

	void Start ()
	{
		uiManager = guiManager.GetComponent<UIManager> ();

		// At the initialization we must make sure that the ball we are going to throw doesn't fall on our fingers

		// Add the is kinematic variable.

		ball.isKinematic = true;

		// Disable the ball camera.

		CameraManager cm = cameraMan.GetComponent <CameraManager> ();//ballCamera.enabled = false;

		// Make sure that the camera is on the player.

		cm.ShowCharacterCamera();
	}

	// Update is called once per frame should also be able to pick up a ball.

	void Update () 
	{
		if (!uiManager.GetUITabStatus ())
		{
			// Call the function that checks if the fire button has been pressed.

			CheckForFireButton ();
		}
	}

	void CheckForFireButton()
	{
		if (!uiManager.GetAimUIStatus ())
		{
			// Check if the user pressed the button in order to throw the ball. 

			if (Input.GetButtonUp ("Fire1") && ball.isKinematic == true) // Fire1 stands for left ctrl or left click
			{
				// We need to show the ui in order to adjust the force and the spin.

				uiManager.ShowAimUI ();
			}
		}
		else
		{
			// If the player presses right click it cancels tha throw.

			if (Input.GetButtonUp ("Fire2"))
			{
				uiManager.HideAimUI ();
			}
			else if (Input.GetButtonUp ("Fire1"))
			{
				//Vector3 forwardDirection = ball.transform.TransformDirection (Vector3.forward);

				// Remove the is kinematic variable.

				ball.isKinematic = false;

				// Hide the corsshair.

				//crosshair.enabled = false;

				// We need to set the direction that we want to fire the bullet. In order to do so we make a vector for it to follow. The created vector repressents the forward movement from this possition.

				Vector3 forwardDirectionTheMomentOfThrow = transform.TransformDirection (Vector3.forward); //transform.forward

				// Add the velocity to the ball

				//print (forwardDirectionTheMomentOfThrow* throwPower);

				//ball.AddTorque(ball.transform.up * leftSpinSlider.value); // We want that.

				float spin = leftSpinSlider.value - rightSpinSlider.value;

				// Get the throw force.

				throwPower = forceSlider.value;

				//ball.AddTorque(ball.transform.up * spin);

				ball.AddForce (forwardDirectionTheMomentOfThrow * throwPower,ForceMode.Impulse);

				ball.SendMessage ("ThrowTheBall");

				// Enable the ball camera.

				CameraManager cm = cameraMan.GetComponent <CameraManager> ();//ballCamera.enabled = false;

				cm.ShowBowlingBallCamera();
				//ballCamera.enabled = true;	

				forceSlider.value = forceSlider.minValue;

				leftSpinSlider.value = leftSpinSlider.minValue;

				rightSpinSlider.value = rightSpinSlider.minValue;

				uiManager.HideAimUI ();
			}
		}
	}

	public void ManageSpinSliders()
	{
		
	}
}
