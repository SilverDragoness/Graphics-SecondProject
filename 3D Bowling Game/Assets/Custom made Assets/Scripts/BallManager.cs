﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallManager : MonoBehaviour 
{
	public GameObject ballLauncher; // In order to know where to place the ball.
	public GameObject ballCamera;
	public GameObject cameraMan;
	public GameObject alley;
	bool isThrown;
	bool isOnAlley;

	// Use this for initialization

	void Start () 
	{
		BallReset ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		// Check if the ball is thrown.

		if (!isThrown)
		{
			// If its not make sure that it follows the lancher and in extend the player.

			transform.position = ballLauncher.transform.position;
		}
	}
		
	void OnTriggerEnter(Collider objectCollider)
	{
		if (objectCollider.gameObject.tag == "CameraStopper")//isBeingHit == false)
		{
			ballCamera.GetComponent<BallCameraController>().follow = false;

			// Also inform the alley

			alley.GetComponent<AlleyManager> ().ballEnteredNoCameraArea ();
		}
	}


	void OnCollisionEnter(Collision withObject)
	{
		//Debug.Log (transform.localRotation);
		if (withObject.gameObject.name == "Alley")
		{
			// Play a sound.

			GetComponent<AudioSource> ().Play ();
		}
	}

	// This function is called in order to inform the ball that is being thrown.

	void ThrowTheBall()
	{
		// Set the boolean.

		isThrown = true;
	}

	void PickUpTheBall()
	{
		// Set the boolean.

		isThrown = false;

		// Disable the ball camera.

		CameraManager cm = cameraMan.GetComponent <CameraManager> ();//ballCamera.enabled = false;

		cm.ShowCharacterCamera();

		// Reset the camera.

		ballCamera.GetComponent<BallCameraController>().follow = true;
	}

	// This function is responsible for the kegels reset.

	void BallReset()
	{
		//Debug.Log ("Ball Reset!");

		// In order for the reset to be complite we must also remove any other forces applied to the kegels or else they will fall over again.

		GetComponent<Rigidbody> ().velocity = Vector3.zero;
		GetComponent<Rigidbody> ().angularVelocity = Vector3.zero;	

		// And dont let the ball escape!

		GetComponent<Rigidbody> ().isKinematic = true;

		// Set the rotation of the kegel.

		transform.localRotation = Quaternion.Euler (35.0f, 45.0f, 176.0f);// Vector4(0.0,0.0,0.0,0.1);

		// Set the position of the kegel based on the variables.

		transform.position = ballLauncher.transform.position;

		// And also se the boolean so the ball doesnt think its trown.

		PickUpTheBall ();
	}

	public bool GetThrownStatus()
	{
		return isThrown;
	}
}
