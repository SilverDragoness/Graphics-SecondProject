﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KegelManager : MonoBehaviour 
{
	// This class is responisble for the kegels wearabouts. 
	// Knows if the kegel is being hit or knock down and also resets the kegel back to its apropriate rotation and position.

	// Public Variables

	//public AudioClip hitSound;
	
	// Private Variables

	bool isBeingHit;
	bool isDown;
	Vector3 kegelInitialLocalPos;
	Animation resetAnimation;

	// Use this for initialization

	void Start () 
	{
		isBeingHit = false;
		isDown = false;
		kegelInitialLocalPos = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);

	}
	// So at start or reset we can se the local rotation of each one to (0.0,0.0,0.0,0.1)
	// The local rotation when the pins are in the correct possition is the ones added to the inspector
	// Update is called once per frame
	void Update () 
	{
		//Debug.Log (transform.localRotation);
	}

	void OnCollisionEnter(Collision withObject)
	{
		//Debug.Log (transform.localRotation);
		if (withObject.gameObject.name == "Ball")//isBeingHit == false)
		{
			//Debug.Log ("boom!");

			// Play a sound.

			//GetComponent<AudioSource> ().Play ();

			isBeingHit = true;
		}


		// Check if the kegel has fallen.

		if (transform.localRotation != Quaternion.Euler (0.0f, 0.0f, 0.0f))
		{
			// Then the kegel has fallen and we must inform the boolean.

			isDown = true;
		}

			
	}

	// This function is responsible for the kegels reset.

	//void KegelReset(Vector3 coordinates)//float x, float y, float z)
	public void KegelReset()
	{
		// In order for the reset to be complite we must also remove any other forces applied to the kegels or else they will fall over again.

		GetComponent<Rigidbody> ().velocity = Vector3.zero;
		GetComponent<Rigidbody> ().angularVelocity = Vector3.zero;	
	
		//Debug.Log ("Kegel Reset!");

		// Set the rotation of the kegel.

		transform.localRotation = Quaternion.Euler (0.0f, 0.0f, 0.0f);// Vector4(0.0,0.0,0.0,0.1);

		// Set the position of the kegel based on the variables.

		transform.localPosition = kegelInitialLocalPos;//coordinates;//new Vector3 (x, y, z); 

		// The kegel isn't down anymore.

		isDown = false;

		// And set active.

		GetComponent<Rigidbody> ().isKinematic = false;
	}

	// This function is responsible for the removal of fallen kegel.

	public void KegelClean()
	{
		// Check if the kegel has fallen. If so it must be removed.

		if (isDown)
		{
			// Disable the kegel.

			GetComponent<Rigidbody> ().isKinematic = true;

			// Set the rotation of the kegel.

			transform.localRotation = Quaternion.Euler (0.0f, 0.0f, 0.0f);// Vector4(0.0,0.0,0.0,0.1);

			// Set the position of the kegel based on the variables.

			transform.localPosition = new Vector3 (kegelInitialLocalPos.x, kegelInitialLocalPos.y + 0.5f, kegelInitialLocalPos.z);//coordinates;//new Vector3 (x, y, z);
		}
		else
		{
			// In order for the reset to be complite we must also remove any other forces applied to the kegels or else they will fall over again.

			KegelReset ();
		}
	}

	// This function is called in order to return the kegels status.

	public bool GetStatus()
	{	
		return isDown;
	}
		
}
