﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScorePresenter: MonoBehaviour
{
	public Text playerName;
	public Text[] scoreThrowNumberArray;
	public Text[] scoreFrameNumberArray;
	public Sprite normalFrameImage;
	public Sprite selectedFrameImage;

	int throwCounter;
	int frameCounter;
	string normalFontColorHex;
	string selectedFontColorHex;
	public Color normalFontColor; //new Color(,1);
	public Color selectedFontColor; //new Color(,1);


	// Use this for initialization

	void Start ()
	{
		Restart ();
	}

	public void Restart ()
	{
		Initialization ();
		throwCounter = 0;
		frameCounter = 0;
	}

	// Update is called once per frame
	void Update () 
	{
		
	}

	// This method is called in order to initialize the variables and the arrays.

	void Initialization()
	{

		// Set all the texts to empty at the starting of the game.

		for(int i = 0;i <scoreThrowNumberArray.GetLength(0); i++)
		{
			scoreThrowNumberArray [i].text = "";
		}

		for(int i = 0;i <scoreFrameNumberArray.GetLength(0); i++)
		{
			scoreFrameNumberArray [i].text = "";
		}
	}

	// This function is called in order to activate the current player frame.

	public void ActivateFrame()
	{
		// Get the current frame image.

		Image currentImage = gameObject.GetComponent<Image> ();

		// And set it to selected.

		currentImage.sprite = selectedFrameImage;

		// And do more things with the fonts?

		playerName.color = selectedFontColor;

		for(int i = 0;i <scoreThrowNumberArray.GetLength(0); i++)
		{
			scoreThrowNumberArray[i].color = selectedFontColor;
		}

		for(int i = 0;i <scoreFrameNumberArray.GetLength(0); i++)
		{
			scoreFrameNumberArray[i].color  = selectedFontColor;
		}/**/
	}

	// This function is called in order to activate the current player frame.

	public void DeactivateFrame()
	{
		// Get the current frame image.

		Image currentImage = gameObject.GetComponent<Image> ();

		// And set it to deselected.

		currentImage.sprite = normalFrameImage;

		// And do more things with the fonts?
	
		playerName.color = normalFontColor;

		for(int i = 0;i <scoreThrowNumberArray.GetLength(0); i++)
		{
			scoreThrowNumberArray[i].color = normalFontColor;
		}

		for(int i = 0;i <scoreFrameNumberArray.GetLength(0); i++)
		{
			scoreFrameNumberArray[i].color = normalFontColor;
		}
	}
		
		
	public void SetupPlayerName(string name)
	{
		playerName.text = name;
	}

	// This function will be used in order to inform the score manager about the current frame.

	public void UpdateThrowValue(int numberOfKegelsKnockedDown)
	{
		//print ("Throw counter: " + throwCounter);

		CheckForSpare (numberOfKegelsKnockedDown);

		CheckForStrike (numberOfKegelsKnockedDown);

		throwCounter += 1;
			
	}

	// This method checks for strike.

	void CheckForStrike(int numberOfKegelsKnockedDown)
	{
		//Debug.Log ("Check for strike throw counter: " + throwCounter);

		if (numberOfKegelsKnockedDown == 10)
		{
			if (throwCounter < 18)
			{
				throwCounter += 1;

				scoreThrowNumberArray [throwCounter].text = "X";
			}
			else
			{
				scoreThrowNumberArray [throwCounter].text = "X";
			}
			//frameCounter +=1;
		}
		else
		{
			scoreThrowNumberArray [throwCounter].text = numberOfKegelsKnockedDown.ToString();
		}
	}


	void CheckForSpare(int numberOfKegelsKnockedDown)
	{
		// Always check for the second throw.

		if ((throwCounter % 2)!=0)
		{
			int previousThrouw; 

			if ((((int.TryParse (scoreThrowNumberArray [throwCounter - 1].text, out previousThrouw)) ? previousThrouw : 0) + numberOfKegelsKnockedDown) == 10)//(int.Parse(scoreThrowNumberArray [throwCounter].text) + numberOfKegelsKnockedDown) == 10)
			{
				Debug.Log (previousThrouw);
				scoreThrowNumberArray [throwCounter].text = "/";
			}
			else
			{
				scoreThrowNumberArray [throwCounter].text = numberOfKegelsKnockedDown.ToString();
			}
		}
	}

	public void UpdateFrameScore(int[] scoreArray)
	{

		// The first frame must be handled a bit differently.

		if (frameCounter == 0)
		{ 
			if (scoreArray [0].ToString () != "10")
			{
				scoreFrameNumberArray [0].text = (scoreArray [0].ToString () == "0") ? "" : scoreArray [0].ToString ();
			}
		}


		// For the rest of the frames.

		for(int i = 0;i <frameCounter; i++)
		{
			//Debug.Log ("FrameScore: " + scoreArray[i]);
			if (scoreArray [i].ToString () != "10")
			{
				scoreFrameNumberArray [i].text = (scoreArray [i].ToString () == "0") ? "" : scoreArray [i].ToString ();
			}
		}

		if (frameCounter < 10)
		{
			frameCounter +=1;
		}
	}

	public void UpdateFinalFrame(int finalScore)
	{
		scoreFrameNumberArray [9].text = finalScore.ToString();
	}
}

