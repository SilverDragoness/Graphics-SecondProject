﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	//public CameraManager cm;
	public GameObject scoreScreenObject;
	public GameObject scoreGUIObject;
	public GameObject aimGUIObject;
	public Slider forceSlider;
	public Slider leftSpinSlider;
	public Slider rightSpinSlider;
	public Text restartText;
	public Text winnerText;

	bool isTabActive;
	bool isAimActive;
	bool isGameOver;

	// Use this for initialization
	void Start () 
	{
		Cursor.visible = false;
		isTabActive = false;
		isGameOver = false;
		HideAimUI ();
		HideRestartText ();
		HideWinnerText ();
	}

	void Update()
	{
		// Check if a game over flag has been raised.

		if (isGameOver)
		{
			ShowTabUI();
			ShowRestartText ();
			ShowWinnerText ();
		}
		else
		{
			HideRestartText ();
			HideWinnerText ();
			CheckScoreUITab ();
			CheckForSliderInput ();
		}
			
		//CheckAimUITabInput ();
	}

	// This function is called when the user hold the Tab button in order to show a UI Element.

	void CheckScoreUITab() 
	{
		// Check if the user pressed the button in order to throw the ball. 
		//Input.GetButton

		if (Input.GetKey(KeyCode.Tab))//(Input.GetButton("Fire3")) // Fire3 stands for left shift.
		{
			ShowTabUI ();
		}
		else
		{
			HideTabUI ();
		}
	}


	void CheckForSliderInput()
	{
		if (isAimActive)
		{
			if (Input.GetKey (KeyCode.Space))
			{
				if (forceSlider.value == forceSlider.maxValue)
				{
					forceSlider.value = forceSlider.minValue;
				}

				forceSlider.value += 40 * Time.deltaTime;
			}

			if (Input.GetKey (KeyCode.LeftArrow))
			{
				// If the user presses the left arrow and already has a value on the right slider it must first be netrualized.

				if (rightSpinSlider.value > 0)
				{
					rightSpinSlider.value -= 100 * Time.deltaTime;
				}
				else
				{
					leftSpinSlider.value += 100 * Time.deltaTime;
				}

				//if (leftSpinSlider.value == leftSpinSlider.maxValue)
				//{
				//	leftSpinSlider.value = leftSpinSlider.minValue;
				//}

				//leftSpinSlider.value += 100 * Time.deltaTime;
			}

			if (Input.GetKey (KeyCode.RightArrow))
			{
				if (leftSpinSlider.value > 0)
				{
					leftSpinSlider.value -= 100 * Time.deltaTime;
				}
				else
				{
					rightSpinSlider.value += 100 * Time.deltaTime;
				}
			}
		}
	}


	public void ShowTabUI() 
	{
		// Show the UI Tab.

		scoreGUIObject.SetActive (true);

		isTabActive = true;

		Cursor.visible = true;
	}

	public void HideTabUI() 
	{
		scoreGUIObject.SetActive (false);

		isTabActive = false;

		//Cursor.visible = false;
	}

	public void ShowAimUI() 
	{
		aimGUIObject.SetActive (true);
		isAimActive = true;
		Cursor.visible = true;
	}

	public void HideAimUI() 
	{
		aimGUIObject.SetActive (false);
		isAimActive = false;
		Cursor.visible = false;
	}

	public void ShowRestartText() 
	{
		restartText.enabled = true;
	}

	public void HideRestartText() 
	{
		restartText.enabled = false;
	}

	public void ShowWinnerText() 
	{
		winnerText.enabled = true;
	}

	public void HideWinnerText() 
	{
		winnerText.enabled = false;
	}

	public void SetupWinnerText(string text)
	{
		winnerText.text = text;
	}

	public bool GetUITabStatus()
	{
		return isTabActive;
	}

	public bool GetAimUIStatus()
	{
		return isAimActive;
	}

	public void SetGameOver(bool status)
	{
		isGameOver = status;
	}
		
}
